package co.com.bancolombia.jpa;

import co.com.bancolombia.jpa.model.AccountJpa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

public interface AccountJPARepository extends CrudRepository<AccountJpa, Integer>, QueryByExampleExecutor<AccountJpa> {
}
