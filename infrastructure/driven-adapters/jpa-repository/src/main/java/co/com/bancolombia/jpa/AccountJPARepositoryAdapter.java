package co.com.bancolombia.jpa;

import co.com.bancolombia.jpa.helper.AdapterOperations;
import co.com.bancolombia.jpa.model.AccountJpa;
import co.com.bancolombia.model.account.Account;
import co.com.bancolombia.model.account.gateways.AccountRepository;
import org.reactivecommons.utils.ObjectMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountJPARepositoryAdapter extends AdapterOperations<Account, AccountJpa, Integer, AccountJPARepository>
implements AccountRepository
{

    public AccountJPARepositoryAdapter(AccountJPARepository repository, ObjectMapper mapper) {
        super(repository, mapper, d -> mapper.map(d, Account.class));
    }

    @Override
    public List<Account> getAccounts() {
        return super.findAll();
    }

    @Override
    public Account getAccountById(Integer accountNumber) {
        return super.findById(accountNumber);
    }
}
