package co.com.bancolombia.jpa.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@Table(name = "empleador_empleado")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AccountJpa {

    @Id
    private Integer accountNumber;
    private Integer accountType;
}
